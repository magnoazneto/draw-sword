extends Node2D
"""
	Mostra os scores do jogadores na tela.
	
	A partir de um dado valor do número do jogador, 1 ou 2, da quantidade de duelos e dos scores dos
	jogadores são atualizados os pontos mostrados na tela.
"""

onready var point_img : Sprite = $PointImg
onready var img_control : Node2D = $ImgControl

export var player_number: int = 1

func _ready() -> void:
	create_score()
	

func _process(delta) -> void:
	update_score()
	

func create_score() -> void:
	"""Cria as imagens do score de acordo com a quantidade máxima de partidas"""
	for i in range(int(Global.game["max_matches"])):		
		point_img.scale = Vector2(0.05, 0.05)
		match player_number:
			1:
				point_img.position = Vector2((point_img.texture.get_width()*point_img.scale.x)*i, 0)
			2:
				point_img.position = Vector2((point_img.texture.get_width()*point_img.scale.x)*-i, 0)
		img_control.add_child(point_img.duplicate())


func update_score() -> void:
	"""Muda a cor do sprite de acordo com a pontuação do jogador"""
	var values: Array = _get_player_score()
	for result in range(len(values)):
		match values[result]:
			"win":
				img_control.get_child(result).modulate = Color(0,1,0)
			"lose":
				img_control.get_child(result).modulate = Color(1,0,0)
				
				
func _get_player_score() -> Array:
	return Global.players["score"]["p" + str(player_number)]
