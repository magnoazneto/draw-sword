extends Node2D
class_name Player
"""
Cena do jogador durante a partida.

Nota:
	O score do jogador é registrado como array para ser possível ter um controle
	do resultado de cada rodada.
"""
export (int, 1, 2) var number
export var CHAR_CTRL: PackedScene
var char_ctrl : CharacterControl

var game_timer: Timer setget set_game_timer
var time: float = 0
var reaction_time : float

var inputs: Array = []
var inputs_count: int = 0
var input_released := false
var max_inputs: int = 3 setget set_max_inputs
onready var input_controller : InputController = $InputController

var score: Array = []
var wins := 0
var loses := 0
var draws := 0

var character_id: int = 0


func _ready() -> void:
	char_ctrl = CHAR_CTRL.instance()
	set_player()
	set_controller()


func _process(delta):
	if input_controller.is_any_dir_released():
		input_released = true


func set_player() -> void:
	"""Configurações iniciais do jogador levando em conta o número dele"""
	char_ctrl.show_char(Global.players["current_character"]["p"+str(number)])			
#	character_id = Global.players["current_character"]["p"+str(number)]
	set_character()
	score = Global.players["score"]["p"+str(number)]
	if number == 2:		
		char_ctrl.flip_char(Global.players["current_character"]["p"+str(number)])
			

func reset_vars():	
	inputs = []
	inputs_count = 0
	input_released = false
	reaction_time = 0

func get_inputs() -> void:
	"""Leitura dos inputs levando em consideração o número do jogador.
	A quantidade máxima de inputs é determidada durante o script do jogo a partir da quantidade
	de opções definidas no jogo."""
	if inputs_count < max_inputs:
		if inputs_count == 0:
			append_inputs()
		elif input_released:
			append_inputs()
		input_controller.reset_actions()


func append_inputs():
	if input_controller.UP:
		inputs.append(3)
	elif input_controller.DOWN:
		inputs.append(0)
	elif input_controller.LEFT:
		inputs.append(1)
	elif input_controller.RIGHT:
		inputs.append(2)
		
	if input_controller.is_any_dir_pressed():
		input_released = false
		inputs_count += 1
#		if inputs_count == max_inputs:
#			time = game_timer.wait_time - game_timer.time_left - 2
#			time = float(str("%.1f"%time))
#			Global.players["min_time"]["p" + str(number)] = time if time < Global.players["min_time"]["p" + str(number)] \
#																else Global.players["min_time"]["p" + str(number)]


func show_time() -> void:
	"""Mostra o tempo de reação do jogador"""
	if time != 0.0:
		$TimeLabel.text = str("%.1f"%time)
	else:
		$TimeLabel.text = str("tempo excedido")


func set_result(result) -> void:
	"""Mostra o resultado final para o jogador"""
	$WinLoseLabel.text = result
	if result == "lose":
		pass
	update_score(result)


func equals_inputs(op_values: Array) -> bool:
	"""Compara os insputs do jogador com os inputs gerados pelo jogo"""
	if len(inputs) != max_inputs:
		return false
	else:
		for i in range(max_inputs):
			if inputs[i] != op_values[i]:
				return false
	return true


func update_score(result: String) -> void:
	"""Faz update da variável global do score"""
	if result != "draw":
		Global.players["score"]["p" + str(number)].append(result)


func check_win(other_player: Player) -> void:
	"""Para fazer: refatorar as checagens de vitória no script do jogo"""
	pass


func set_controller():
	input_controller.player_number = number

func set_character():
	var character : Character = char_ctrl.get_child(Global.players["current_character"]["p"+str(number)])
	character.show_state("Idle")
	if number == 2:
		character.flip_img()
	$Character.add_child(character.duplicate())

func set_game_timer(timer: Timer) -> void:
	game_timer = timer

func set_max_inputs(value) -> void:
	max_inputs = value
