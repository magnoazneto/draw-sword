extends Node

const TRANS = Tween.TRANS_SINE
const EASE = Tween.EASE_IN_OUT

var origin = 0
var amplitude = 0

onready var player = get_parent()


func start(amplitude = 16):
	self.origin = player.position
	self.amplitude = amplitude
	_new_shake()
	$Finish.start()

func _new_shake():
	var rand = rand_range(-amplitude, amplitude)

	$ShakeTween.interpolate_property(player, "position", player.position, Vector2(player.position.x + rand, player.position.y), 0.01, TRANS, EASE)
	$ShakeTween.start()

func _reset():
	$ShakeTweenBack.interpolate_property(player, "position", player.position, origin, 0.01, TRANS, EASE)	
	$ShakeTweenBack.start()

func _on_ShakeTween_tween_all_completed():
	_reset()

func _on_ShakeTweenBack_tween_all_completed():
	_new_shake()

func _on_Finish_timeout():
	$ShakeTween.stop_all()
	$ShakeTweenBack.stop_all()

