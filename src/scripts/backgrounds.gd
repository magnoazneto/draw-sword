extends Control
"""Cena de controle de backgrounds"""

export (bool) var show_random := true

func _ready() -> void:
	randomize()
	if show_random:
		show_random_sprite()
	
func show_random_sprite() -> void:	
	get_child(randi() % get_child_count()).visible = true
	
	
func get_random_sprite() -> Node:
	return get_child(randi() % get_child_count())
	
	
func show_sprite(idx : int) -> void:
	get_child(idx).visible = true
