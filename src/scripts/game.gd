extends Control
"""
Cena onde acontecem os duelos.

Nota:
	Algumas das variáveis dos jogadores, bgs e dos duelos são guardadas pela cena Global
	já que em toda rodada a cena do duelo é reiniciada.
"""

export (String, FILE) var start_screen_path

var game_started: bool = false
var input_allowed: bool = false
var draw: bool = false
var bar_draw_allowed: bool = false

var _players_start_pos: Array = [Vector2(), Vector2()] setget _set_players_start_pos

onready var sounds: Node = $AllSounds
onready var options: Node = $OptionsController
onready var game_timer: Timer = $Time/Timer

onready var player_one: Player = $Players/Player1
onready var player_two: Player = $Players/Player2


func _ready() -> void:
	_set_background()
	player_one.set_game_timer(game_timer)
	player_two.set_game_timer(game_timer)
	_set_players_start_pos([player_one.position, player_two.position])
	_start_round()


func _physics_process(delta) -> void:
	_inputs_controller()
	if bar_draw_allowed: draw_controller()
	show_debug()
	

func _inputs_controller() -> void:
	"""Cena de controle e checagem dos inputs do jogadores"""
	if input_allowed and game_started:
		player_one.max_inputs = options.amount_options
		player_one.get_inputs()
		
		player_two.max_inputs = options.amount_options
		player_two.get_inputs()
		
		if (player_one.time != 0 and player_two.time != 0):
			game_timer.stop()
			check_winner(player_one, player_two, options.values)
	else:
		allow_inputs()


func draw_controller() -> void:
	"""Cena de controle da barra de empate"""
	var bar_one: Sprite = $BarDraw/BarOne
	var bar_two: Sprite = $BarDraw/BarTwo
	options.show_values(options.values, "hide")
	bar_one.modulate = "0034ff"
	bar_two.modulate = "03ff36"
	bar_one.show()
	bar_two.show()
	if (Input.is_action_just_pressed("ui_draw_one") or Input.is_action_just_pressed("joy_1_any_button")) and not bar_one.scale.x >= 30:
		bar_one.scale.x += 0.5
		bar_one.position.x += 8
		bar_two.scale.x -= 0.5
		bar_two.position.x += 8
	if (Input.is_action_just_pressed("ui_draw_two") or Input.is_action_just_pressed("joy_2_any_button")) and not bar_two.scale.x >= 30:
		bar_one.scale.x -= 0.5
		bar_one.position.x -= 8
		bar_two.scale.x += 0.5
		bar_two.position.x -= 8


func _start_round() -> void:
	game_timer.start()
	game_started = true


func allow_inputs() -> void:
	if game_timer.time_left <= 8 and not input_allowed:
		options.show_values(options.values, "show")
		input_allowed = true


func check_winner(p1,p2,op) -> void:
	"""Checagem de resultados do duelo"""
	game_started = false
	
	if p1.equals_inputs(op) and p2.equals_inputs(op):
		if p1.time > p2.time:
			show_result(true, false)
			sounds.get_random_sound().play()
		elif p2.time > p1.time:
			show_result(false, true)
			sounds.get_random_sound().play()
		else:
			draw = true
			sounds.get_random_sound().play()
			if Global.is_bar_draw_allowed():
				bar_draw_allowed = true
				$BarDraw/DrawTimer.start()
			show_result(true, true)

	elif p1.equals_inputs(op):
		sounds.get_random_sound().play()
		show_result(false, true)
		
	elif p2.equals_inputs(op):
		sounds.get_random_sound().play()
		show_result(true, false)
		
	else:
		show_result(true, true)
	
	if not draw: Global.duel["number_rounds"] += 1


func show_result(p1_lost, p2_lost) -> void:
	"""Mostra resultado final do duelo"""
	animation_hit(p1_lost, p2_lost)
	if p1_lost and p2_lost:
		player_one.set_result("draw" if draw else "lose")
		player_two.set_result("draw" if draw else "lose")
		player_one.show_time()
		player_two.show_time()
	
	else:
		player_one.set_result("win" if p2_lost else "lose")
		player_two.set_result("win" if p1_lost else "lose")
	if not bar_draw_allowed: $TimeFinishBattle.start()


func animation_hit(p1_lost, p2_lost) -> void:
	"""Cena de controle de animação de golpe e flash"""
	var flash: CanvasModulate = $FlashTime
	if not p1_lost or not p2_lost and not draw:
		_call_animation()
	elif draw:
		_call_animation()
	flash.start()
		


func show_debug() -> void:
	"""Mostrar alguns valores do jogo.	
		#ToDo: Deixar esta função na cena Global
	"""
	if Global.debug:
		$Debug.text = "Time left: " + str(game_timer.time_left) + \
					  "\nScore: " + str(Global.players["score"]) + \
					  "\nOptions values: " + str(options.values) + \
					  "\nP1 inputs: " + str(player_one.inputs) + \
					  "\nP2 inputs: " + str(player_two.inputs) + \
					  "\nExceeded: " + str(Global.duel["exceeded"]) + \
					  "\nMin time: " + str(Global.players["min_time"])


func _call_animation() -> void:
	"""Cena de chamada de animação de ataque"""
	var hit_tween: Tween = $HitTween
	hit_tween.interpolate_property($Players/Player1, "position", null, Vector2(600, 500), 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	hit_tween.interpolate_property($Players/Player2, "position", null, Vector2(700, 500), 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	hit_tween.start()


func _on_draw_timer_timeout() -> void:
	"""Controle do resultado do evento de empate"""
	var bar_one: Sprite = $BarDraw/BarOne
	var bar_two: Sprite = $BarDraw/BarTwo
	if bar_one.scale.x > bar_two.scale.x:
		player_one.set_result("win")
		player_two.set_result("lose")
	elif bar_one.scale.x < bar_two.scale.x:
		player_one.set_result("lose")
		player_two.set_result("win")

	bar_draw_allowed = false
	Global.duel["number_rounds"] += 1
	$TimeFinishBattle.start()


func _on_timer_timeout() -> void:
	"""Chamado quando o tempo da partida foi estourado"""
	Global.duel["exceeded"] += 1
	check_winner(player_one, player_two, options.values)


func _on_time_finish_battle_timeout() -> void:
	"""Chamado ao final de cada duelo"""
	if Global.is_game_over():
		get_tree().change_scene(start_screen_path)
	else:
		get_tree().reload_current_scene()


func _on_hit_tween_tween_all_completed() -> void:
	if not bar_draw_allowed:
		var hit_back_tween: Tween = $HitBackTween
		hit_back_tween.interpolate_property($Players/Player1, "position", null, _players_start_pos[0], 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		hit_back_tween.interpolate_property($Players/Player2, "position", null, _players_start_pos[1], 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
		hit_back_tween.start()
	else:
		player_one.get_node("Shake").start()
		player_two.get_node("Shake").start()


func _set_players_start_pos(positions: Array) -> void:
	"""Salva as posições iniciais do jogadores"""
	_players_start_pos = [positions[0], positions[1]]
	
	
func _on_hit_back_tween_tween_all_completed() -> void:
	player_one.show_time()
	player_two.show_time()


func _set_background() -> void:
	$Backgrounds.show_sprite(Global.duel["game_bg_idx"])
