extends Node2D
class_name MusicPlayer
"""Controle das músicas"""

export var on: bool = true
export var random: bool = true
export (float, EXP, -80, 24, 0.5) var volume : float = 0

var current_music_idx : int = 0

#Variáveis de controle
var last_on := !on
var last_volume := volume


func _ready() -> void:
	_loop_off()
	

func _process(delta) -> void:
	_toggle()
	_update_volume()


func play(music_idx : int) -> void:
	for child in get_children():
		if child.playing:
			return
	current_music_idx = music_idx
	get_child(current_music_idx).play()


func next() -> void:
	if current_music_idx < get_child_count() - 1:
		current_music_idx += 1
	else:
		current_music_idx = 0
	stop()
	play(current_music_idx)


func previous() -> void:
	if current_music_idx > 0:
		current_music_idx -= 1
	else:
		current_music_idx = get_child_count() - 1
	stop()
	play(current_music_idx)


func stop() -> void:
	for child in get_children():
		if child.playing:
			child.stop()


func play_random() -> void:
	randomize()
	play(randi() % get_child_count())


func _loop_off() -> void:
	"""Assegura que nenhuma música irá ficar em loop"""
	for child in get_children():
		child.stream.loop = false


func _toggle() -> void:
	"""On/Off de música"""
	if _has_updated(on, last_on):
		if !on:
			stop()
		else:
			if random:
				play_random()
			else:
				play(current_music_idx)
		last_on = on


func _update_volume() -> void:
	if _has_updated(volume, last_volume):
		for child in get_children():
			child.volume_db = volume
		last_volume = volume


func _has_updated(var1, var2) -> bool:
	"""
	Checa se as variáveis foram atualizadas.
	Nota: Normalmente utilizado para evitar loops desnecessários.
	"""
	return var1 != var2
