extends Node2D
"""
Script para atualização das imagens de acordo com os inputs do jogadores.

Lê os inputs dos jogadores e atualiza os ponteiros de seleção e as imagens dos personagens
de acordo com a escolha de cada jogador.
"""

signal players_ready

var current_char: Dictionary = {"p1":0, "p2": 0}
var has_choosen: Dictionary = {"p1":false, "p2": false}

onready var chars_sel: Node2D = get_parent()
onready var chars_ctrl: CharacterControl = chars_sel.get_node("CharacterControl")

func _ready() -> void:
	Global.reset_game_vars()	
	add_control()
	
	
func _process(delta) -> void:
	update_mark()
	update_chars()
	update_selection()
	check_choosen()
	
	
func update_mark() -> void:
	"""Atualiza a posição do ponteiro de cada jogador"""
	$Player1/Mark.global_position = Vector2(
								chars_sel.get_icon(current_char["p1"]).global_position.x, 
								chars_sel.get_icon(current_char["p1"]).global_position.y + 50)
	
	$Player2/Mark.global_position = Vector2(
								chars_sel.get_icon(current_char["p2"]).global_position.x, 
								chars_sel.get_icon(current_char["p2"]).global_position.y - 50)


func update_chars() -> void:
	"""Atualiza a imagem de cada jogador"""
	$Player1/CharPos.get_child(0).show_char(current_char["p1"])
	$Player2/CharPos.get_child(0).show_char(current_char["p2"])
	

func add_control() -> void:
	"""Adiciona a cena CharacterControl como filho do node CharPos de cada jogador
	e atualiza os valores para o player 2."""
	$Player1/CharPos.add_child(chars_ctrl.duplicate())
	$Player2/CharPos.add_child(chars_ctrl.duplicate())
	$Player2/CharPos.get_child(0).flip_all_char()
	current_char["p2"] = chars_ctrl.get_child_count() -1


func update_selection() -> void:
	"""Checagem dos inputs dos jogadores e controle dos valores de seleção"""
	if !has_choosen["p1"]:
		if Input.is_action_just_pressed("ui_d"):
			if current_char["p1"] >= chars_ctrl.get_child_count() - 1:
				current_char["p1"] = 0
			else:
				current_char["p1"] += 1
		elif Input.is_action_just_pressed("ui_a"):
			if current_char["p1"] > 0:
				current_char["p1"] -= 1
			else:
				current_char["p1"] = chars_ctrl.get_child_count() - 1
		
		if Input.is_action_just_pressed("ui_space") or Input.is_action_just_pressed("joy_1_any_button"):
			$Player1/Mark.modulate = Color(0.5,0,0)
			has_choosen["p1"] = true
			
	if !has_choosen["p2"]:
		if Input.is_action_just_pressed("ui_right"):
			if current_char["p2"] >= chars_ctrl.get_child_count() - 1:
				current_char["p2"] = 0
			else:
				current_char["p2"] += 1
		elif Input.is_action_just_pressed("ui_left"):
			if current_char["p2"] > 0:
				current_char["p2"] -= 1
			else:
				current_char["p2"] = chars_ctrl.get_child_count() - 1
		
		if Input.is_action_just_pressed("ui_enter") or Input.is_action_just_pressed("joy_1_any_button"):
			$Player2/Mark.modulate = Color(0,0,0.5)
			has_choosen["p2"] = true


func check_choosen() -> void:
	"""Checa se ambos jogadores da escolheram. Caso estiverem escolhidos emite um sinal."""
	if (has_choosen["p1"] and has_choosen["p2"]):
		Global.players["current_character"]["p1"] = current_char["p1"]
		Global.players["current_character"]["p2"] = current_char["p2"]
		emit_signal("players_ready")
