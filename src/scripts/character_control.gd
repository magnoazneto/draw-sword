extends Node2D
class_name CharacterControl
"""Controle de personagem.

Cena de controle de personagens onde é usada em várias cenas.

Nota:
	Todos os personagens devem ser uma instância da cena Character."""


func ready() -> void:
	randomize()
	

func show_char(idx : int) -> void:
	"""Liga a visibilidade do personagem pelo o id e desliga os demais."""
	for i in get_child_count():
		if i == idx:
			get_child(i).get_node("States").visible = true
		else:
			get_child(i).get_node("States").visible = false


func flip_char(idx: int) -> void:
	"""Inverte a posição do personagem por id"""	
	for state in get_child(idx).get_node("States").get_children():
		state.flip_h = state.flip_h


func flip_all_char() -> void:
	"""Inverte a posição de todos os personagens"""
	for i in get_child_count():
		flip_char(i)


func show_random() -> void:
	get_child(randi() % get_child_count()).get_node("State").visible = true


func get_icon(idx : int) -> Node:
	return get_child(idx).get_node("Icon")


func get_idx_by_name(name : String) -> int:
	for i in range(get_child_count()):
		if get_child(i).name == name:
			return i
	return 0
