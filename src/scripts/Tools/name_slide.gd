extends HBoxContainer

#player 1
onready var p1_stripe := $P1/Stripe
onready var p1_name_ctrl := $P1/Name
var p1_stripe_start_pos_x : float
var p1_name_ctrl_start_pos_x : float

#player 2
onready var p2_stripe := $P2/Stripe
onready var p2_name_ctrl := $P2/Name
var p2_stripe_start_pos_x : float
var p2_name_ctrl_start_pos_x : float

var p1_last_name = "Name"
var p2_last_name = "Name"

func _ready():
	p1_stripe_start_pos_x = p1_stripe.rect_global_position.x
	p1_name_ctrl_start_pos_x = p1_name_ctrl.rect_global_position.x
	
	p2_stripe_start_pos_x = p2_stripe.rect_global_position.x
	p2_name_ctrl_start_pos_x = p2_name_ctrl.rect_global_position.x


func _process(delta):
	slide_to_center()


func slide_to_center():
	p1_stripe.rect_global_position.x = lerp(p1_stripe.rect_global_position.x, 0, 0.2)
	p1_name_ctrl.rect_global_position.x = lerp(p1_name_ctrl.rect_global_position.x, 0, 0.1)
	
	p2_stripe.rect_global_position.x = lerp(p2_stripe.rect_global_position.x, Global.PROJECT_WIDTH - 480, 0.2)
	p2_name_ctrl.rect_global_position.x = lerp(p2_name_ctrl.rect_global_position.x, Global.PROJECT_WIDTH - 480, 0.1)


func _on_Selection_mark_updated(p1_name, p2_name):
	if p1_name != p1_last_name:
		p1_last_name = p1_name
		p1_name_ctrl.get_node("Label").text = p1_last_name
		p1_stripe.rect_global_position.x = p1_stripe_start_pos_x
		p1_name_ctrl.rect_global_position.x = p1_name_ctrl_start_pos_x
	
	if p2_name != p2_last_name:
		p2_last_name = p2_name
		p2_name_ctrl.get_node("Label").text = p2_last_name
		p2_stripe.rect_global_position.x = p2_stripe_start_pos_x
		p2_name_ctrl.rect_global_position.x = p2_name_ctrl_start_pos_x
