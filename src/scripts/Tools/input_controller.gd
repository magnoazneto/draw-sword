extends Node
class_name InputController

export (int,1,2) var player_number := 1

#actions
var LEFT := false
var RIGHT := false
var UP := false
var DOWN := false
var START := false

var ANY_ACTION := false
var ANY_DIR := false


func _ready() -> void:
	pass


func handle_inputs() -> void:
	LEFT = Input.is_action_just_pressed("ui_p{num}_left".format({"num":player_number}))
	RIGHT = Input.is_action_just_pressed("ui_p{num}_right".format({"num":player_number}))
	UP = Input.is_action_just_pressed("ui_p{num}_up".format({"num":player_number}))
	DOWN = Input.is_action_just_pressed("ui_p{num}_down".format({"num":player_number}))
	START = Input.is_action_just_pressed("ui_p{num}_start".format({"num":player_number}))	
	ANY_DIR = is_any_action_pressed()
	ANY_ACTION = is_any_action_pressed()


func is_any_dir_pressed() -> bool:
	for dir in [LEFT, RIGHT, UP, DOWN]:
		if dir:
			return true
	return false
	

func is_any_action_pressed() -> bool:
	for action in [is_any_dir_pressed(), START]:
		if action:
			return true
	return false

func is_any_dir_released() -> bool:
	var dirs = ["left", "right", "up", "down"]
	for dir in dirs:
		if Input.is_action_just_released("ui_p{num}_{dir}".format({"num":player_number, "dir":dir})):
			return true
	return false


func _input(event) -> void:
	handle_inputs()

func reset_actions() -> void:
	LEFT = false
	RIGHT = false
	UP = false
	DOWN = false
	START = false
	ANY_ACTION = false
	ANY_DIR = false
