extends HBoxContainer

signal slide_finished


#player 1
onready var p1_stripe := $P1/Stripe
onready var p1_labels_ctrl := $P1/Labels
var p1_stripe_start_pos_x : float
var p1_labels_ctrl_start_pos_x : float

#player 2
onready var p2_stripe := $P2/Stripe
onready var p2_labels_ctrl := $P2/Labels
var p2_stripe_start_pos_x : float
var p2_labels_ctrl_start_pos_x : float

var p1_last_name = "Name"
var p2_last_name = "Name"

var _show := false

func _ready():
	set_process(false)
	p1_stripe_start_pos_x = p1_stripe.rect_global_position.x
	p1_labels_ctrl_start_pos_x = p1_labels_ctrl.rect_global_position.x
	
	p2_stripe_start_pos_x = p2_stripe.rect_global_position.x
	p2_labels_ctrl_start_pos_x = p2_labels_ctrl.rect_global_position.x


func _process(delta):
	if _show:
		slide_show()
	else:
		slide_hide()


func slide_show():
	p1_stripe.rect_global_position.x = lerp(p1_stripe.rect_global_position.x, 0, 0.2)
	p1_labels_ctrl.rect_global_position.x = lerp(p1_labels_ctrl.rect_global_position.x, 0, 0.1)
	
	p2_stripe.rect_global_position.x = lerp(p2_stripe.rect_global_position.x, Global.PROJECT_WIDTH - 480, 0.2)
	p2_labels_ctrl.rect_global_position.x = lerp(p2_labels_ctrl.rect_global_position.x, Global.PROJECT_WIDTH - 480, 0.1)

func slide_hide():
	p1_stripe.rect_global_position.x = lerp(p1_stripe.rect_global_position.x, p1_stripe_start_pos_x, 0.2)
	p1_labels_ctrl.rect_global_position.x = lerp(p1_labels_ctrl.rect_global_position.x, p1_labels_ctrl_start_pos_x, 0.1)
	
	p2_stripe.rect_global_position.x = lerp(p2_stripe.rect_global_position.x, p2_stripe_start_pos_x, 0.2)
	p2_labels_ctrl.rect_global_position.x = lerp(p2_labels_ctrl.rect_global_position.x, p2_labels_ctrl_start_pos_x, 0.1)


func _on_NewGame_result_ready(p1, p2):
	_show = true
	
	if p1.reaction_time > 0:
		p1_labels_ctrl.get_node("Result").text = Global.players["score"]["p1"].back()
		p1_labels_ctrl.get_node("Time").visible = true
		p1_labels_ctrl.get_node("Time").text = str(p1.reaction_time)
	elif p1.reaction_time == -1:
		p1_labels_ctrl.get_node("Result").text = "time execeeded!"
		p1_labels_ctrl.get_node("Time").visible = false
	else:
		p1_labels_ctrl.get_node("Result").text = "miss"
		p1_labels_ctrl.get_node("Time").visible = false
	
	if p2.reaction_time > 0:
		p2_labels_ctrl.get_node("Result").text = Global.players["score"]["p2"].back()
		p2_labels_ctrl.get_node("Time").visible = true
		p2_labels_ctrl.get_node("Time").text = str(p2.reaction_time)
	elif p2.reaction_time == -1:
		p2_labels_ctrl.get_node("Result").text = "time execeeded!"
		p2_labels_ctrl.get_node("Time").visible = false
	else:
		p2_labels_ctrl.get_node("Result").text = "miss"
		p2_labels_ctrl.get_node("Time").visible = false
		
	set_process(true)
	$Wait.start()


func _on_Wait_timeout():
	_show = false
	$End.start()

func _on_End_timeout():
	emit_signal("slide_finished")
	set_process(false)
