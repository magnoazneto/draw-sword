extends Control

export (String, FILE) var next_scene_path

var winner_name : String
var player_one : Player
var player_two : Player

func _ready():
	set_process(false)
	set_process_input(false)
	modulate.a = 0

func _process(delta):
	modulate.a = lerp(modulate.a, 1, 0.2)

func _input(event):
	if player_one.get_node("InputController").START or player_two.get_node("InputController").START:
		get_tree().change_scene(next_scene_path)

func _on_NewGame_result_ready(p1, p2):
	player_one = p1
	player_two = p2
	winner_name = "nobody"
	var players = [p1, p2]
	if p1.wins == p2.wins:
		for player in players:
			if player.loses < max(p1.loses, p2.loses):
				winner_name = player.get_node("Character").get_child(0).char_name
				break
	elif p1.wins == 0 and p2.wins == 0:
		$Label.text = "no winner!"
		return
	else:
		for player in players:
				if player.wins == max(p1.wins, p2.wins):
					winner_name = player.get_node("Character").get_child(0).char_name
					break
					
	$Label.text = winner_name + " won!"
	

func _on_NewGame_finished():
	set_process_input(true)
	set_process(true)
