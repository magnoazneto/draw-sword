extends Node2D
"""
Autoload de controle de cenas e variáveis do jogo.
"""

#Variáveis do projeto
var PROJECT_WIDTH: float = ProjectSettings.get_setting("display/window/size/width")

export var background: PackedScene
export var char_ctrl: PackedScene

#Status do jogo
var game: Dictionary = {
	"tickets":0,
	"session_time":0,
	"max_matches":5
}

#Variáveis do duelo
var duel: Dictionary = {
	"duel_time":0,
	"game_bg_idx": null,
	"number_rounds": 0,
	"exceeded":0
}

#Variáveis dos jogadores
var players: Dictionary = {
	"current_character":{"p1":0, "p2":0},
	"score":{"p1":[], "p2":[]},
	"min_time":{"p1":10, "p2":10}
	}


#Debug
export var debug: bool = false


func _ready() -> void:
	reset_game_vars()


func _process(delta) -> void:
	pass


func is_game_over() -> bool:
	"""Retorna se algumas condições de fim de jogo foram satisfeitas"""
	for player in players["score"].keys():
		if duel["number_rounds"] >= game["max_matches"]: return true
		if players["score"][player].count("win") >= int(game["max_matches"] / 2) + 1:
			return true
		elif duel["exceeded"] >= 3:
			return true
	return duel["number_rounds"] >= game["max_matches"]


func is_bar_draw_allowed() -> bool:
	"""Retorna se é permitido aparecer a barra de draw"""
	if players["score"]["p1"].count("win") == 2 or players["score"]["p2"].count("win") == 2:
		return true
	elif duel["number_rounds"] >= int(game["max_matches"] / 2) + 1:
		return true
	return false
	

func reset_game_vars() -> void:
	"""Atribuir valores padrão para algumas variáveis"""
	duel["game_bg_idx"] = rand_child_num(background.instance())
	set_players_char_id()
	duel["duel_time"] = 0
	duel["number_rounds"] = 0
	duel["exceeded"] = 0
	players["score"] = {"p1":[], "p2":[]}
	
	
static func rand_child_num(node: Node) -> int:
	"""Retorna um número aleatório levando em conta a quantidade de filhos do Node"""
	randomize()
	return randi() % node.get_child_count()
	
	
func set_players_char_id() -> void:
	"""Atribui um valor aleatório para o id dos personagens dos jogadores"""
	var chars= char_ctrl.instance()
	players["current_character"]["p1"] = rand_child_num(chars)
	players["current_character"]["p2"] = rand_child_num(chars)
	
