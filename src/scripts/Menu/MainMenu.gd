extends Control

export (String, FILE) var selection_menu : String

func _ready():
	randomize()
	Global.reset_game_vars()
	set_random_anim()
	

func _process(delta):
	$GlowTween.play($PressStart)
	
	if is_Start_pressed():
		_on_Button_pressed()

func _on_Button_pressed():
	get_tree().change_scene(selection_menu)


func is_Start_pressed() -> bool:
	return $Players/P1InputController.START or $Players/P2InputController.START
		

func set_random_anim():
	var char_anim = $CharAnim 
	for anim in char_anim.get_children():
		anim.visible = false
		anim.playing = false
	var rand_idx = randi() %char_anim.get_child_count()
	char_anim.get_child(rand_idx).visible = true
	char_anim.get_child(rand_idx).play()
