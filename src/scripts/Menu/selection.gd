extends Control

signal mark_updated(p1_name, p2_name)

export var CHAR_CTRL : PackedScene
export (String, FILE) var next_scene_path : String


#character control
var char_ctrl : CharacterControl

#icons
var icons := []

#players mark
onready var p1_mark := $Mark/P1
onready var p2_mark := $Mark/P2
var p1_mark_idx := 0
var p2_mark_idx := 2

#players input
onready var p1_input := $PlayersInput/P1
onready var p2_input := $PlayersInput/P2

#countdown
onready var countdown_label = $Countdown/Label
onready var countdown_time = $Countdown/Timer

func _ready() -> void:
	char_ctrl = CHAR_CTRL.instance()
	clear_all_blanks(self)
	add_icons()
	update_img()
	emit_signal("mark_updated", 
				char_ctrl.get_child(p1_mark_idx).char_name, 
				char_ctrl.get_child(p2_mark_idx).char_name)
	

func _process(delta) -> void:
	check_ready()
	update_player_mark()
	update_countdown()


func clear_all_blanks(node: Node) -> void:	
	for i in node.get_child_count():
		var child = node.get_child(i)
		if "Blank" in child.name:
			child.modulate.a = 0
		clear_all_blanks(child)


func add_icons() -> void:
	var icons_grid = $OuterRows/CenterColumns/Icons/GridContainer	
	for i in range(icons_grid.get_child_count()):
		var char_name = icons_grid.get_child(i).name
		if !("Blank" in char_name):
			var icon = char_ctrl.get_icon(char_ctrl.get_idx_by_name(char_name)).duplicate()
			icons.append(icon)
			icons_grid.get_child(i).replace_by(icon)


func update_img() -> void:
	var img_1 = char_ctrl.get_child(p1_mark_idx).get_node("States/Idle").duplicate()
	var img_2 = char_ctrl.get_child(p2_mark_idx).get_node("States/Idle").duplicate()
	img_2.flip_h = true
	$Players/VBoxContainer.get_child(1).replace_by(img_1)
	$Players/VBoxContainer2.get_child(1).replace_by(img_2)


func update_player_mark() -> void:
	p1_mark.global_position = icons[p1_mark_idx].get_node("Inside").rect_global_position + \
								+ Vector2(14,17)
	p2_mark.global_position = icons[p2_mark_idx].get_node("Inside").rect_global_position + \
								icons[p2_mark_idx].get_node("Inside").rect_size - Vector2(15,16)

func move_mark() -> void:
	var last_pos := char_ctrl.get_child_count() - 1
	if p1_input.RIGHT:		
		p1_mark_idx = p1_mark_idx + 1 if p1_mark_idx < last_pos else 0
	elif p1_input.LEFT:
		p1_mark_idx = p1_mark_idx - 1 if p1_mark_idx > 0 else last_pos
	elif p1_input.DOWN:
		p1_mark_idx = last_pos if p1_mark_idx != last_pos else 1
	elif p1_input.UP:
		p1_mark_idx = 1 if p1_mark_idx != 1 else last_pos
		
	if p2_input.RIGHT:
		p2_mark_idx = p2_mark_idx + 1 if p2_mark_idx < last_pos else 0
	elif p2_input.LEFT:
		p2_mark_idx = p2_mark_idx - 1 if p2_mark_idx > 0 else last_pos
	elif p2_input.DOWN:
		p2_mark_idx = last_pos if p2_mark_idx != last_pos else 1
	elif p2_input.UP:
		p2_mark_idx = 1 if p2_mark_idx != 1 else last_pos	
	update_player_mark()
	emit_signal("mark_updated", char_ctrl.get_child(p1_mark_idx).char_name, char_ctrl.get_child(p2_mark_idx).char_name)

func select_char() -> void:
	if p1_input.is_processing_input() and p1_input.START:
		p1_input.set_process_input(false)
		p1_mark.modulate = p1_mark.modulate - Color(0.4,0.4,0.4)
		p1_mark.modulate.a = 1
	
	if p2_input.is_processing_input() and p2_input.START:
		p2_input.set_process_input(false)
		p2_mark.modulate = p2_mark.modulate - Color(0.4,0.4,0.4)
		p2_mark.modulate.a = 1


func check_ready():
	if p1_input.START and p2_input.START:
		Global.players["current_character"]["p1"] = p1_mark_idx
		Global.players["current_character"]["p2"] = p2_mark_idx
		countdown_time.paused = true
		if $WaitTime.is_stopped():
			$WaitTime.start()


func update_countdown():	
	countdown_label.text = "%.1f" % countdown_time.time_left


func _input(event):
	move_mark()
	select_char()
	update_img()
	

func _on_Countdown_timeout():
	p1_input.START = true
	p2_input.START = true
	select_char()


func _on_WaitTime_timeout():
	get_tree().change_scene(next_scene_path)
