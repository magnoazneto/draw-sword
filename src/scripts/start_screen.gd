extends Control
"""
Menu principal.

Mostra opções iniciais do jogo, créditos restantes, versão e melhores ranks.

Nota:
	Algumas funcionalidades tais como mostrar créditos e trocar para tela ociosa
	serão implementados.
"""

export (String, FILE) var char_menu

onready var char_ctrl: CharacterControl = $MenuOptions/CenterRow/CenterContainer/CharacterControl
onready var character: TextureRect = $Menu
onready var best_time: Label = $MenuOptions/BestTime


func _ready() -> void:
	Global.reset_game_vars()
	_show_stats()


func _process(delta) -> void:
	_skip()


func get_min_player_time() -> float:
	"""Retorna o melhor resultado da Sessão"""
	return min(Global.players["min_time"]["p1"], Global.players["min_time"]["p2"])


func _show_stats() -> void:
	"""Mostra os melhores resultados e outros detalhes sobre os jogadores"""	
	char_ctrl.flip_all_char()
	char_ctrl.show_random()
	best_time.text = "Best time: " +str(get_min_player_time())
	

func _skip() -> void:
	if Input.is_action_just_pressed("joy_1_any_button") \
		or Input.is_action_just_pressed("joy_2_any_button") \
		or Input.is_action_just_pressed("ui_space"):
		_on_Start_pressed()


func _on_Start_pressed() -> void:
	"""Troca de cena para o menu de seleção de personagens"""
	get_tree().change_scene(char_menu)
