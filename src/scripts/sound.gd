extends Node2D
"""Controle dos sons"""

func get_random_sound() -> Node:
	randomize()
	return get_children()[randi() % (get_child_count())]