extends CanvasModulate
"""Efeito de flash durante a partida"""

var flashes: int = 3
onready var timer: Timer = $Timer

func start() -> void:
	self.visible = true
	timer.start()


func _on_flash_timer_timeout() -> void:
	self.visible = not self.visible
	flashes -= 1
	if flashes > 0:
		timer.start()
