extends Node2D

"""
Script para fazer o controle dos sprites e valores das posições que os jogadores devem acertar.
"""

export var options: PackedScene

export (int, 1, 5) var amount_options: int = 3
var values: Array

func _ready() -> void:
	add_options(amount_options)
	values = rand_op_list(amount_options)
	

func add_options(amount) -> void:
	"""Adcionar os filhos e ajustar a posição de acordo com a quantidade entrada"""
	var temp = int(amount / 2);
	var space_length = 200
	for i in range(amount):
		var option = options.instance()
		add_child(option)
		
		#ajustar posição das ultimas opções no node
		for o in get_child(get_child_count() - 1).get_children(): 
			o.global_position.x = Global.PROJECT_WIDTH/2
			o.position += Vector2((space_length) * (i - temp), 0)
			if amount % 2 == 0:
				o.position += Vector2(space_length/2, 0)


func show_all() -> void:
	"""Mostra todos os sprites adicionados"""
	for c in get_children():
		for i in c.get_children():
			i.show()

func hide_all():
	for c in get_children():
		for i in c.get_children():
			i.hide()

func set_values(vals : Array):
	values = vals

func show_values(values: Array, method: String) -> void:
	"""Mostra os valores de acordo com a lista dada como parametro"""
	set_values(values)
	for i in range(get_child_count()):
		get_child(i).get_child(values[i]).show() if method == "show" else get_child(i).get_child(values[i]).hide()
		
		
func rand_op_list(amount: int) -> Array:
	"""Retorna uma lista com valores aleatórios de acordo com a quantidade definida"""
	randomize()
	var result_list = []
	for i in range(amount):
		result_list.append(randi() % 4)
	return result_list
	
