extends Node2D
"""
Controle de seleção de personagens.

Cena que recebe os nodes referentes à seleção de personagens:
	-> CharacterControl: cena que recebe todos os personagens
	-> PlayerSelection: cena que atualiza as imagens de acordo com os inputs dos jogadores
"""

export var game_scene: PackedScene
export var dist_between_icons: int = 128

onready var char_ctrl: CharacterControl = $CharacterControl

func _ready() -> void:
	show_char_icons()	


func show_char_icons() -> void:
	"""Distribui os ícones na tela"""
	var temp = int(char_ctrl.get_child_count()/2)
	for i in char_ctrl.get_child_count():
		var character_icon = char_ctrl.get_child(i).get_node("Icon")
		character_icon.visible = true
		character_icon.global_position.x = Global.PROJECT_WIDTH/2
		character_icon.position += Vector2((dist_between_icons) * (i - temp), 0)
		if char_ctrl.get_child_count() % 2 == 0:
			character_icon.position += Vector2(dist_between_icons/2, 0)


func get_icon(idx : int) -> Node:
	return char_ctrl.get_icon(idx)
	

func _on_WaitTime_timeout() -> void:
	"""Tempo de espera após seleção"""
	get_tree().change_scene(game_scene.resource_path)


func _on_PlayerSelection_players_ready() -> void:
	$WaitTime.start() if $WaitTime.is_stopped() else null
