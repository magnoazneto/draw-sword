extends Control

signal result_ready(p1, p2)
signal finished

export var CHAR_CTRL : PackedScene

var _players_start_pos: Array = [Vector2(), Vector2()] setget _set_players_start_pos
onready var player_one := $Players/P1/PlayerClass
onready var player_two := $Players/P2/PlayerClass
var player_one_reaction_time : float
var player_two_reaction_time : float

onready var options_ctrl := $Options/OptionsController
var current_options : Array

onready var wait_timer = $Timers/Wait
onready var round_timer := $Timers/Round
var reaction_time :float = 0

var rounds_count := 0

func _ready():
	set_process_input(false)
	_set_players_start_pos([player_one.global_position, player_two.global_position])
	set_players()
	

func _process(delta):
	_update_reaction_time()
		

func _input(event):
	player_one.get_inputs()
	player_two.get_inputs()
	check_players_inputs()


func set_players():
	player_one.set_max_inputs(options_ctrl.amount_options)
	player_two.set_max_inputs(options_ctrl.amount_options)


func check_players_inputs():
	for player in [player_one, player_two]:
		if player.is_processing_input():
			if player.inputs_count == options_ctrl.amount_options:
				if _equals_arrays(player.inputs, current_options):
					player.reaction_time = reaction_time
				player.set_process_input(false)
				
	if !player_one.is_processing_input() and !player_two.is_processing_input():
		check_results()

func check_results():
	var players = [player_one, player_two]
	for i in range(len(players)):
		var other_player = players[(len(players)-1) -i]
		if !_equals_arrays(players[i].inputs, current_options):
			Global.players["score"]["p" + str(i+1)].append("lose")
			players[i].loses += 1
		elif other_player.reaction_time > 0:
			# é possível um jogador ficar com o tempo de reação 0 ou -1
			if players[i].reaction_time < other_player.reaction_time:
				Global.players["score"]["p" + str(i+1)].append("win")
				players[i].wins += 1
			else:
				Global.players["score"]["p" + str(i+1)].append("lose")
				players[i].loses += 1
		elif _equals_arrays(players[i].inputs, current_options):
			Global.players["score"]["p" + str(i+1)].append("win")
			players[i].wins += 1
		else:
			Global.players["score"]["p" + str(i+1)].append("lose")
			players[i].loses += 1
			
	round_timer.stop()
	change_player_state()
	emit_signal("result_ready", player_one, player_two)	
	end_round()


func change_player_state():
	var players = [player_one, player_two]
	for i in range(len(players)):
		if Global.players["score"]["p" + str(i+1)].back() == "win":
			players[i].get_node("Character").get_child(0).show_state("Attack")
		elif Global.players["score"]["p" + str(i+1)].back() == "lose":
			players[i].get_node("Character").get_child(0).show_state("Defeat")
	

func end_round():
	options_ctrl.hide_all()
	player_one.reset_vars()
	player_two.reset_vars()
	set_process_input(false)


func is_game_finished() -> bool:
	var players = [player_one, player_two]
	var max_win_lose = round(Global.game["max_matches"] / 2) +1
	if rounds_count >= Global.game["max_matches"]:
		return true
	for player in players:
		if player.wins >= max_win_lose or player.loses >= max_win_lose:
			return true
	return false


func _on_Start_timeout():
	rounds_count += 1
	set_process_input(true)
	player_one.set_process_input(true)
	player_two.set_process_input(true)
	current_options = options_ctrl.rand_op_list(options_ctrl.amount_options)
	options_ctrl.show_values(current_options, "show")
	round_timer.start()


func _update_reaction_time():
	if !round_timer.is_stopped():
		reaction_time = round_timer.wait_time - round_timer.time_left
		

func _on_Round_timeout():
	for player in [player_one, player_two]:
		if player.is_processing_input():
			#caso o tempo do round acabar e o jogador não tiver completado os inputs
			player.reaction_time = -1
	check_results()
	player_one.set_process_input(false)
	player_two.set_process_input(false)
	wait_timer.start()
	

func _has_values(arr1: Array, arr2: Array) -> bool:
	var min_len = min(len(arr1), len(arr2))
	for i in range(min_len):
		if arr1[i] != arr2[i]:
			return false
	return true

func _equals_arrays(arr1: Array, arr2: Array) -> bool:
	if len(arr1) != len(arr2):
		return false
	return _has_values(arr1, arr2)


func _set_players_start_pos(positions: Array) -> void:
	"""Salva as posições iniciais do jogadores"""
	_players_start_pos = [positions[0], positions[1]]


func _on_ScoreSlide_slide_finished():
	if is_game_finished():
		$ScoreSlide.visible = false
		$Scores.visible = false
		emit_signal("result_ready", player_one, player_two)
		emit_signal("finished")
	else:
		player_one.get_node("Character").get_child(0).show_state("Idle")
		player_two.get_node("Character").get_child(0).show_state("Idle")
		$Timers/Start.start()
