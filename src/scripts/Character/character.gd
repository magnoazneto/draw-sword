extends Node2D
class_name Character
"""
Cena para controle dos recursos de personagem
"""

export (String) var char_name : String

func _ready() -> void:
	_default_visibility()
	show_state("Idle")


func _default_visibility() -> void:
	$Icon.visible = false
	$States.visible = true
	for state in $States.get_children():
		state.visible = false

func flip_img():
	for img in $States.get_children():
		img.flip_h = !img.flip_h

func show_state(state_name : String = "Idle") -> void:
	for state in $States.get_children():
		if state_name.to_lower() == state.name.to_lower():
			state.visible = true
		else:
			state.visible = false
