extends Tween

var _dim : bool = true

func _ready():
	pass

func play(node: Node) -> void:
	if !is_active():
		if _dim:
			interpolate_property(
				node, "modulate", node.modulate, Color(0.2,0.2,0.2), 
				_rand_time(), Tween.TRANS_BOUNCE, Tween.EASE_OUT_IN
			)
		else:
			interpolate_property(
				node, "modulate", node.modulate, Color(1.5,1.5,1.5), 
				_rand_time(), Tween.TRANS_BOUNCE, Tween.EASE_OUT_IN
			)
		_dim = !_dim
		start()

func _rand_time() -> float:
	randomize()
	return rand_range(1,3)
